# -*- coding: utf-8 -*-

"""Tests for the stream_recorder module."""
import os
from pathlib import Path
from unittest import TestCase

import envoy
import pendulum
from freezegun import freeze_time

from record import get_date_and_time_prefix


class RecordStreamTestCase(TestCase):
    """Test case for some aspects of the main module."""

    def test_01_get_datetime_string(self):  # noqa: D102
        with freeze_time(pendulum.datetime(2020, 1, 22, 10, 59, 33, tz='Europe/Berlin')):  # noqa: WPS432
            self.assertEqual(get_date_and_time_prefix(), '20200122_105933_')

    def test_02_small_script_run(self):  # noqa: WPS210
        """Test a full run."""
        current_folder = Path(__file__).parent.as_posix()
        commandline_run_result = envoy.run(
            (
                f'python record.py --stream-url http://www.memoryradio.de:4000 --duration 1 '
                f'--output-folder {current_folder!s} --broadcast-name Test'
            ),
        )
        self.assertEqual(
            0,
            commandline_run_result.status_code,
        )
        dir_list = os.listdir(current_folder)
        mp3_count = 0
        current_year = pendulum.now().year
        for file_name in dir_list:
            if str(file_name).endswith('Test.mp3') and str(file_name).startswith(str(current_year)):  # noqa: WPS221
                mp3_count += 1
                os.remove(file_name)
        self.assertEqual(1, mp3_count)
